# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


=begin
user = User.create!(name:  'Maxim Korenev',
              email: 'unzoom1@gmail.com',
              roles: 'linux, unix, openstack',
              values: '',
              website: 'https://www.linkedin.com/in/maggnus')
user.save              
              
user = User.create!(name:  'Alex Borodach',
              email: 'alex.borodach1@gmail.com',
              roles: '9 years of software development in different fields. Well proficient in client-side development as well as have extensive experience in server-side development. During my career, worked at such companies as Epam Systems, Yandex and Google. Participated in projects ranging from developing front-end and server parts of web apps with complex user interaction flows and data models to programming ‘big data’ analysis systems and visualization of huge amount of detected objects. Primary skills: Web, JavaScript (FullStack), C++',
              values: 'I\'d like to join any ambitious enough startup on a technical role.',
              website: 'https://www.linkedin.com/in/alexborodach')
user.save              
              
user = User.create!(name:  'Alex',
              email: 'alex1@deppkind.com',
              roles: 'Hi, currently i am an self employed IT entrepreneur, CEO/CTO/TeamLead @ Deppkind llc, i offering contract rails development and it advocating with 10+ years of IT experience. http://www.deppkind.com/about/ale',
              values: 'SF / Miami based sport-tech (american football) startup. Relocation with my wife.',
              website: 'http://ru.linkedin.com/in/deppkind')
user.save            
=end
