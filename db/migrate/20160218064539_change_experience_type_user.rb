class ChangeExperienceTypeUser < ActiveRecord::Migration
  def change
    change_column(:users, :experience, :text)
    change_column(:users, :educ, :text)
  end
end
