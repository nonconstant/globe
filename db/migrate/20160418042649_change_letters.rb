class ChangeLetters < ActiveRecord::Migration
  def change
    change_column :letters, :skills, :text
  end
end
