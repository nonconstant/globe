class AddDetailsToOffers < ActiveRecord::Migration
  def change
    change_table(:offers) do |t|
      t.belongs_to :user, index: true
    end
  end
end
