class ChangeTests < ActiveRecord::Migration
  def change
    remove_column :tests, :type
    add_column :tests, :num, :integer
  end
end
