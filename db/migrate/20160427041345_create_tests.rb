class CreateTests < ActiveRecord::Migration
  def change
    create_table :tests do |t|
      t.belongs_to :user, index: true
      t.integer :type
      t.string :answers
      t.integer :timelimit
      t.integer :total
      t.integer :maxtotal

      t.timestamps null: false
    end
  end
end
