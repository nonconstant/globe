class AddDetailsToAuthorizations < ActiveRecord::Migration
  def change
    add_column :authorizations, :location, :string
  end
end
