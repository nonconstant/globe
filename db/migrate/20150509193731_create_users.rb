class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :skype
      t.string :linkedin
      t.string :roles
      t.text :values
      t.string :places
      t.integer :status

      t.timestamps null: false
    end
  end
end
