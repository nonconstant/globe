class CreateLetters < ActiveRecord::Migration
  def change
    create_table :letters do |t|
      t.text :text
      t.string :header
      t.integer :category

      t.timestamps null: false
    end
  end
end
