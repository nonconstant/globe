class AddDetailsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :resume, :string
    remove_column :users, :linkedin
    remove_column :users, :places
  end
end
