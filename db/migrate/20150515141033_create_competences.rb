class CreateCompetences < ActiveRecord::Migration
  def change
    create_table :competences do |t|
      t.string :title
      t.string :synonyms

      t.timestamps null: false
    end
  end
end
