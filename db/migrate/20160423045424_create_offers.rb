class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.integer :sender
      t.integer :rec
      t.integer :status
      t.string :role
      t.string :description
      t.string :value

      t.timestamps null: false
    end
  end
end
