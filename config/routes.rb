Rails.application.routes.draw do

  devise_for :users, controllers: { sessions: "users/sessions", registrations: "users/registrations", confirmations: 'users/confirmations', omniauth_callbacks: "users/omniauth_callbacks" }
  
  ##get 'password_resets/new'
  ##get 'password_resets/edit'

  ##get 'sessions/new'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  #root 'static_pages#home'
  root "static_pages#home"
  get 'help', to: 'static_pages#help'
  get 'about', to: 'static_pages#about'
  get 'contacts', to: 'static_pages#contacts'
  get 'admin', to: 'static_pages#admin'
  get 'stats', to: 'static_pages#stats'
  
  devise_scope :user do
    get "business", to: "users/registrations#new_bus"
    get "prosignup", to: "users/registrations#new_pro"
    get 'signup', to: 'users/registrations#new_bus'
    get '/auth/facebook/callback', to: 'users/sessions#create'
    get '/users/auth/facebook/callback', to: 'users/sessions#create'
    get '/auth/linkedin/callback', to: 'users/sessions#create'
    get '/users/auth/linkedin/callback', to: 'users/sessions#create'
    get 'login', to: "users/sessions#new"
  end
  
    post 'createpassword', to:'users#createpassword'
  
  
  #post   'login', to: 'sessions#create'
  #delete 'logout', to: 'sessions#destroy'

  
  
  resources :users do
    collection do
    end
    member do
      put "like", to: "users#upvote"
      put "dislike", to: "users#downvote"
      get "offer", to: "users#newoffer"
      post "offer", to: "users#createoffer"
      get "acceptoffer", to: "users#acceptoffer"
      get "test", to: "users#new_test"
      post "test", to: "users#create_test"
      get "tests", to: "users#tests", as: :tests
      get "applications", to: "users#applications", as: :applications
    end
  end
  

  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :letters, only: [:new, :create, :show]
  post 'sendletter', to: 'letters#create'
  get 'sendletter', to: 'letters#new'
  get 'automate', to: 'letters#automate_new'
  post 'automate', to: 'letters#automate_create'
  get 'automate_show', to: 'letters#automate_show'
  
  get "users/:id/edit_admin", to: "users#edit_admin", as: :edit_user_admin
  post "users/:id/mark_interviewed", to: "users#mark_interviewed", as: :user_mark_interviewed
  
  post 'users', to: 'users#index'
  get "/feed/:query", to: "users#feed"
  post "/feed/:query", to: "users#feed"
  get 'feed', to: 'users#feed'
  post 'feed', to: 'users#feed'
  
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
  
  get 'errors/not_found'
  get 'errors/internal_server_error'
  get "/404", to: "errors#not_found"
  get "/500", to: "errors#internal_server_error"
  get "*path" => 'errors#not_found'
end
