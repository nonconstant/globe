class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :edit_admin, :update, :destroy, :mark_interviewed, :edit_admin, :newoffer, :createoffer, :new_test, :create_test, :tests, :applications, :edit_admin]
  before_action :set_s3_direct_post, only: [:new, :edit, :create, :update, :edit_admin]
  
  before_action :authenticate_user!, only: [:edit, :tests]
  http_basic_authenticate_with name: ENV['LOGIN'], password: ENV['PASSWORD'], only: [:index, :destroy, :admin, :edit_admin, :mark_interviewed]

  rescue_from ActiveRecord::RecordNotFound do |exception|
    redirect_to new_user_session_path, :alert => exception.message
  end

  # GET /users
  # GET /users.json
  def index
    @users = User.all
    @tests = $globalvar_test
  end

  # GET /users/1
  # GET /users/1.jsonrake db:rollback
  def show
    store_location
    @tests = $globalvar_test
    if @user.utype == 0
      render 'person'
    else
      render 'project'
    end
  end
  
  def person
  end
  
  def project
  end
  
  def feed
    @user = nil
    @qskills = Hash.new
    @feed = []
    utype = 1 #by default search for business
    
    if params then
      $globalvar_skills.each do |skill|
        if params[skill] then 
          @qskills[skill] = "on"
        end
      end
    end
    if @qskills.empty? && user_signed_in? then
      @qskills = current_user.skills
    end
    if @qskills&&(@qskills.count>0) then 
      User.where("utype = ? AND visibility=1", 1-utype).order('rating DESC').all.each do |u|
        if u.skills then
          adduser = false
          @qskills.each do |bs|
            u.skills.each do |ps|
              if bs[0].to_s==ps[0].to_s then
                adduser = true
              end
            end
          end
          if adduser then 
            @feed << u
            puts "added user " + u.id.to_s
          end
        end
      end
    end
    if !@user then 
      puts "not logged in"
      render 'feed_business' and return
    elsif
      @user.utype==1 then
      render 'feed_business' and return
    else
      render 'feed_pro' and return
    end
  end
  
  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
    store_location
    begin
      if @user.id != current_user.id then
        flash[:warning] = "You need to log in into this account to edit its profile."
        redirect_back_or current_user
        return
      end
    rescue
      redirect_back_or current_user
      return
    end
      
    if @user.utype == 0
      render 'editperson'
    else
      render 'editproject'
    end
  end
  
  def edit_admin
    if @user.utype == 0
      render 'editperson'
    else
      render 'editproject'
    end
  end
  
  def editperson
  end
  
  def editproject
  end
  
  def mark_interviewed
    @user.update_attribute(:status, 2)
    @user.update_attribute(:interviewed_at, Time.zone.now)
    redirect_to :back
  end
  

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @user.status = 0
    @user.visibility = 1
    if (@user.utype==0) then
      respond_to do |format|
        if @user.save
          @user.calcrating
          @user.send_activation_email
          flash[:success] = "Hey, " + @user.name.to_s + "! You are a part of Globe now! Please check your email to activate your account."
          format.html { redirect_to @user}
          format.json { render :show, status: :created, location: @user }
        else
          format.html { render template: 'static_pages/signup_pro', anchor: 'signup'}
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    else
      respond_to do |format|
        if @user.save
          @user.send_activation_email
          flash[:success] = "Thanks for joining, " + @user.firstname.to_s + "! Please check your email to activate your account."
          format.html { redirect_to @user}
          format.json { render :show, status: :created, location: @user }
        else
          format.html { render template: 'static_pages/signup_business', anchor: 'signup'}
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        @user.save!
        format.html { 
          @user.calcrating
          #add date to update_history
          uh = @user.update_history
          if uh==nil then uh=[] end
          uh << Time.zone.now
          @user.update_attribute(:update_history, uh)
          flash[:success] = "Profile updated"
          redirect_to @user
        }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { 
          flash[:warning] = "Couldn't save updates."
          if @user.utype == 0
            render :editperson
          else
            render :editproject
          end
          
        }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { 
        flash[:info] = 'User was successfully destroyed.'
        redirect_to admin_url }
      format.json { head :no_content }
    end
  end
  
  def tests
    @tests = $globalvar_test
    
  end
  
  def applications
    if @user.utype == 0 then
      render 'pro_applications'
    else
      render 'bus_applications'
    end
  end
  
  def new_test()
    @tests = $globalvar_test
    if params&&params[:test_num] then
      num = params[:test_num].to_i
    else num = 0
    end
    if @user.has_test?(num) && Rails.env.production? then 
      flash[:danger] = "You have already completed this test."
      redirect_back_or @user and return
    end
    @test = Test.new()
    @test.num = num
    @test.maxtotal = @tests[num.to_i][3].count
    @test.timelimit = @tests[num.to_i][2]
    @test.save
    @user.tests << @test
    @user.save
    render 'test0'
  end
  
  def create_test()
    @tests = $globalvar_test
    total = 0
    num = params[:test][:num].to_i
    i=1
    if num then
      @tests[num][3].each do |ans|
        if params["q"+i.to_s] == ans.to_s then
          total+=1
        end
        i+=1
      end
    end
    puts "total = " + total.to_s
    if @user.tests&&params[:test][:id] then
      @test = @user.tests.where("id="+params[:test][:id]).first
      if !@test then
        flash[:danger] = "Error saving your test."
        redirect_to current_user and return
      end
      @test.total = total
      @test.save
      flash[:success] = "Great job! Your results are being processed."
    end
    redirect_to tests_user_path(current_user)
  end
  
  def newoffer()
    @offer = Offer.new()
    @offer.rec = params[:rec]
    @offer.sender = params[:id]
    puts "offer sender " + @offer.sender.to_s + " rec " + @offer.rec.to_s
  end
  
  def createoffer()
    @offer = Offer.new(offer_params)
    @offer.sender = @user.id
    @offer.status=0
    puts "created new offer"
    if @offer.save
      puts "offer has been sent"
      @user.offers << @offer
      puts "offer added"
      @user.save
      puts "user saved"
      rec = User.find(@offer.rec)
      text = ("Congratulations! "+ @user.name + " made you an offer on The Globe. <br> <a href="+login_url+" >Log in</a> to check it out.").html_safe
      UserMailer.send_mail(rec.email, rec.name, "You received an offer", text).deliver_now
      puts "mail sent"
      flash[:success] = "Great job! We've just sent your offer to " + rec.name + "!"
      redirect_to current_user
    else
      puts "offer has not been saved"
      render 'newoffer_user'
    end
  end
  
  def acceptoffer()
    @offer = Offer.find(params[:offer_id])
    if @offer
      if current_user.id == @offer.rec then
        @offer.status=1
        @offer.save
        sender = User.find(@offer.sender)
        rec = User.find(@offer.rec)
        text = ("Congratulations! "+ rec.name + " accepted your offer on The Globe. <br> <a href="+login_url+" >Log in</a> to check it out.").html_safe
        UserMailer.send_mail(sender.email, sender.name, "Your offer is accepted", text).deliver_now
        flash[:success] = "Great job! We've let " + sender.name + " know that you accepted the offer!"
      else
        flash[:error] = "You need to log in to accept this offer."
      end
    end
    redirect_back_or current_user
  end
  
  def upvote()
    receiver = User.find(params[:id])
    if current_user.voted_up_on? receiver
      flash[:success] = "You have already expressed interest in this user."
    else
      receiver.liked_by current_user
      #check if like is mutual, then send an email to 3 parties.
      if receiver.voted_up_on? current_user then
        #send email
        flash[:success] = "Mutual interest expressed! We will initiate a conversation between you and " + receiver.name + " soon."
        current_user.send_mutual_interest_email(receiver)
      else
        current_user.send_interest_email(receiver)
        flash[:success] = "You expressed interest."
      end
    end
    begin
      redirect_to :back
    rescue
      redirect_back_or receiver
    end
  end
  
  def downvote()
    receiver = User.find(params[:id])
    if current_user.voted_up_on? receiver
      receiver.unliked_by current_user
      flash[:info] = "You have unliked the user."
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      puts "Set user, id in params" + params[:id].to_s
      @user = User.find(params[:id])
    end
    
    def set_s3_direct_post
      @s3_direct_post = S3_BUCKET.presigned_post(key: "uploads/#{SecureRandom.uuid}/${filename}", success_action_status: '201', acl: 'public-read')
    end
    

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:resume, :recommended, :utype, :name, :firstname, :email, :visibility, :skype, :phone, :picture, :website, :linkedin_link, :github_link, :facebook_link, :educ, :experience, :location, :citizenship, :values, :places, :status, :tasks, :password, :password_confirmation, :summary, :rating, :linkedin, :likes, :industry, :project, :product, :stop_letters, :cvpublic, :roles, skills: $globalvar_skills)
    end
    # Before filters
    # Confirms a logged-in user.
    def logged_in_user
      if !user_signed_in? 
        store_location
        flash[:warning] = "Please log in."
        redirect_back_or sign_in_path
      end
    end

    # Confirms the correct user.
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
    def offer_params
      params.require(:offer).permit(:rec, :role, :description, :value)
    end
    def test_params
      params.require(:test).permit(:num, :answers, :timelimit)
    end
    
    
end
