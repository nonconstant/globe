class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper
  before_action :configure_permitted_parameters
  def after_sign_in_path_for(resource)
    resource
  end
  def after_inactive_sign_up_path_for(resource)
    if resource.utype == 0 then 
      '/new_pro'
    else
      '/new_bus'
    end
  end
  

  protected

  def configure_permitted_parameters
=begin
    devise_parameter_sanitizer.permit(:sign_up) do |user_params|
      user_params.permit({ skills: $globalvar_skills }, :utype, :name, :firstname, :email, :visibility, :skype, :phone, :picture, :website, :linkedin_link, :github_link, :facebook_link, :educ, :experience, :location, :citizenship, :values, :places, :status, :tasks, :password, :password_confirmation, :summary, :rating, :linkedin, :likes, :industry, :project, :product, :stop_letters, :cvpublic, :roles)
    end
    devise_parameter_sanitizer.permit(:sign_in) do |user_params|
      user_params.permit(:username, :email)
    end
    devise_parameter_sanitizer.permit(:account_update) do |user_params|
      user_params.permit({ skills: $globalvar_skills }, :utype, :name, :firstname, :email, :visibility, :skype, :phone, :picture, :website, :linkedin_link, :github_link, :facebook_link, :educ, :experience, :location, :citizenship, :values, :places, :status, :tasks, :password, :password_confirmation, :summary, :rating, :linkedin, :likes, :industry, :project, :product, :stop_letters, :cvpublic, :roles)
    end
=end
  end
end
