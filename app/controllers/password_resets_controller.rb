class PasswordResetsController < ApplicationController
  
  before_action :get_user,   only: [:edit, :update]
  before_action :valid_user, only: [:edit, :update]
  before_action :check_expiration, only: [:edit, :update]
  
  
  rescue_from ActiveRecord::RecordNotFound do |exception|
    redirect_to login_url, :alert => exception.message
  end
  
  def new
  end

  def create
    @user = User.find_by(email: params[:password_reset][:email].downcase)
    if @user
      @user.create_reset_digest
      begin
        @user.send_password_reset_email
      rescue
        flash[:danger] = "Sorry, email could not be sent at this time. Please contact our administrator at theglobeteam@gmail.com"
          redirect_to :back
      end
      flash[:info] = "Email sent with password reset instructions"
      redirect_to login_path
    else
      flash.now[:danger] = "Email address not found"
      render 'new'
    end
  end

  def edit
    
  end
  
  def update
    if params[:user][:password].empty?
      flash.now[:danger] = "Password can't be empty"
      render 'edit'
    elsif @user.update_attributes(user_params)
      log_in @user
      flash[:success] = "Password has been reset."
      redirect_to @user
    else
      render 'edit'
    end
  end
  
  private
  
  def user_params
    params.require(:user).permit(:password, :password_confirmation)
  end

  def get_user
    @user = User.find_by(email: params[:email])
  end

  # Confirms a valid user.
  def valid_user
    if !@user
      flash[:warning] = "User not found."
      redirect_to new_password_reset_path
    elsif !@user.activated?
      @user.amend_activation_mail
      flash[:warning] = "User not activated. Activation mail resent. If you cannot find it, please contact our administrator at theglobeteam@gmail.com"
      redirect_to new_password_reset_path
    elsif !@user.authenticated?(:reset, params[:id])
      flash[:warning] = "Password reset link is not valid."
      redirect_to new_password_reset_path
    end
  end
  
  # Checks expiration of reset token.
  def check_expiration
    if @user.password_reset_expired?
      flash[:danger] = "Password reset has expired."
      redirect_to new_password_reset_url
    end
  end
  
  
end
