class AccountActivationsController < ApplicationController
  rescue_from ActiveRecord::RecordNotFound do |exception|
    redirect_to login_url, :alert => exception.message
  end
  
  def edit
    user = User.find_by(email: params[:email])
    puts "account activation edit entered"
    if !user
      puts "usr not found"
      flash[:danger] = "Invalid activation link. User not found."
      redirect_to login_path
    elsif user.activated?
      puts "usr already activated"
      flash[:warning] = "Your profile has already been activated. Please log in."
      redirect_to login_path
    elsif user.authenticated?(:activation, params[:id])
      puts "activating user"
      user.activate
      puts "usr activated"
      flash[:success] = "Welcome, "+ user.name + ". You are all set. Please fill in you profile data by clicking on Edit profile link."
      log_in user
      puts "user logged_in"
      redirect_to user_path(user)
    else
      flash[:danger] = "Invalid activation link"
      redirect_to root_url
    end
  end
end
