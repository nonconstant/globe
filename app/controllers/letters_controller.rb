class LettersController < ApplicationController
  before_action :set_letter, only: [:show]
  http_basic_authenticate_with name: ENV['LOGIN'], password: ENV['PASSWORD'], only: [:new]
  
  def automate_new
    @letter = Letter.new
  end
  
  # POST /automate
  def automate_create
    @letterlist = parse_automate_text(letter_params[:text])
    if @letterlist && !@letterlist.empty? then
      @letterlist.each do |letter|
        letter.send_plain_mail()
        puts "letter has been saved"
      end
      render :automate_show
    else
      flash[:danger] = "The text could not be parsed"
      puts "The text could not be parsed"
      render :new
    end
  end
  
  def automate_show
  end
  
  def new
    @letter = Letter.new
  end

  # POST /letter
  def create
    @letter = Letter.new(letter_params)
    if @letter.save
      puts "letter has been saved"
      @letter.send_mail()
      flash[:success] = "Hey, we've just sent out your letter!"
      redirect_to new_letter_path
    else
      puts "letter has not been saved"
      render :new
    end
  end
  
  def show
    
    @header = @letter.header
    @text = @letter.text
    render template: 'user_mailer#send_mail'
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_letter
      puts "Set letter, id in params" + params[:id].to_s
      begin
        @letter = Letter.find(params[:id])
      rescue
       puts "Not found."
        redirect_back_or new_letter
      end
    end
    

    # Never trust parameters from the scary internet, only allow the white list through.
    def letter_params
      params.require(:letter).permit(:header, :text, :category, :start_id, skills: $globalvar_skills)

    end
    
    
  def parse_automate_text(text)
    if !text.lines then return nil end
    list = []
    text.lines.each do |line|
      items = line.split(';')
      if items.count == 4 then
        name = items[0].gsub(/[\'\"]/,'').gsub(/\A | \z/,'')
        lookingfor = items[3].gsub(/[\'\"]/,'').gsub(/\A | \z/,'').gsub(/\r|\r\n|\n/,'')
        q = ""
        items[2].split(',').each do |skill|
          q << "&" << skill.gsub(/[\'\"]/,'').gsub(/\A | \z/,'').gsub(' ','+') << "=on"
        end
        @letter = Letter.new()
        @letter.header = "Working for " + name
        @letter.rec = items[1]
        @letter.text = "Hello<br><br>Hope this email finds you well. I noted that " + name.to_s + " is looking for " + lookingfor.to_s + ", so I wanted to introduce you to a pool of high profile web/mobile engineers from Russia and Eastern Europe willing to travel to US and around the world. What we offer is a contract at around $6k per month and up to 6 months duration with the pro travelling to your location and working in your office. Upon completion you may extend a permanent offer, continue collaborating remotely, or engage another pro &mdash; it is all up to you, we don't interfere in any way. We only charge 10% of the short-term contract. We pre-screen pros and serve as a guarantor for both sides.<br><br>Take a look at some of the profiles that may suit your needs: <a href='http://www.theglobeteam.com/feed?" + q.to_s + "&source=cmc'>http://www.theglobeteam.com/feed?" + q.to_s + "</a><br><Br>If you are willing to give it a try and with any other questions please contact me. Thank you for your time!<br><Br>Best of luck,<br>Irina Nazarova<br>CEO and co-founder<br>The Globe<br><a href='http://theglobeteam.com/?&source=cmc'>theglobeteam.com</a>"
        @letter.save
        list << @letter
      else
        puts "Wrong number of arguments"
      end
    end
    return list
    
  end
end
