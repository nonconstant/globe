class SessionsController < ApplicationController

  
  rescue_from ActiveRecord::RecordNotFound do |exception|
    redirect_to login_url, :alert => exception.message
  end
  
  def new
    if logged_in?
      redirect_to current_user
    end
  end
  

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user then params[:session][:user_id] = user.id end
    
    if user && user.authenticate(params[:session][:password])
      if user.activated?
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        redirect_to user
      else
        message  = "Account not activated. "
        message += "Check your email for activation link."
        flash[:warning] = message
        user.amend_activation_mail
        redirect_to login_url
      end
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end
  
  def destroy
    store_location
    log_out if logged_in?
    redirect_to login_url
  end

end
