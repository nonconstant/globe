class OffersController < ApplicationController
  
  def new
    @offer = Offer.new
  end

  # POST /offer
  def create
    @offer = Offer.new(offer_params)
    if @offer.save
      puts "offer has been sent"
      flash[:success] = "Hey, we've just sent out your offer!"
      redirect_to current_user
    else
      puts "offer has not been saved"
      render :new
    end
  end
  
  def show
    
    @header = @offer.header
    @text = @offer.text
    render template: 'user_mailer#send_mail'
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.

    # Never trust parameters from the scary internet, only allow the white list through.
    def offer_params
      params.require(:offer).permit(:sender, :rec, :role, :description, :value)
    end
end