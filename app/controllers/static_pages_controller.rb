class StaticPagesController < ApplicationController
  http_basic_authenticate_with name: ENV['LOGIN'], password: ENV['PASSWORD'], only: [:admin, :stats]
  rescue_from ActiveRecord::RecordNotFound do |exception|
    redirect_to login_url, :alert => exception.message
  end
  
  def admin
    @users = User.all.order('created_at DESC')
    @votes = ActsAsVotable::Vote.all.order('created_at DESC')
    @tests = Test.all.order('created_at DESC')
  end
  
  def stats
    @users = User.all.order('created_at DESC')
    @votes = ActsAsVotable::Vote.all.order('created_at DESC')
  end
  
  def home
    if current_user then
      redirect_to current_user and return
    else
      redirect_to "/business"
    end
  end
  def signup_pro
    @user = User.new
  end

  def help
  end
  def signup_business
    @user = User.new
  end
  def about
  end
  def contacts
  end
end
