class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  require 'uuidtools'

  def facebook
    oauthorize "Facebook"
  end

  def linkedin
    oauthorize "LinkedIn"
  end

  def passthru
    redirect_to "/404"
  end

private

  def oauthorize(kind)
    puts "uauthorize " + kind.to_s + " " + env["omniauth.auth"].to_s
    @user = find_for_ouath(kind, env["omniauth.auth"], current_user)
    puts "user = " + @user.name
    if @user
      puts "success "
      
      puts env["omniauth.auth"].to_s
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => kind
      session["devise.#{kind.downcase}_data"] = env["omniauth.auth"]
      sign_in_and_redirect @user, :event => :authentication
    end
  end

  def find_for_ouath(provider, access_token, resource=nil)
    user, email, name, uid, auth_attr = nil, nil, nil, {}
    case provider
      when "Facebook"
        uid = access_token['uid']
        email = access_token['info']['email']
        auth_attr = { :uid => uid, :token => access_token['credentials']['token'],
          :secret => nil, :first_name => access_token['info']['first_name'],
          :last_name => access_token['info']['last_name'], :name => access_token['info']['name'],
          :link => access_token['extra']['raw_info']['link'] }
      when 'LinkedIn'
        uid = access_token['uid']
        name = access_token['info']['name']
        auth_attr = { :uid => uid, :token => access_token['credentials']['token'],
          :secret => access_token['credentials']['secret'], :first_name => access_token['info']['first_name'],
          :last_name => access_token['info']['last_name'],
          :location => access_token['info']['location'],
          :link => access_token['info']['urls']['public_profile'] }
    else
      raise 'Provider #{provider} not handled'
    end
    if resource.nil?
      if email
        user = find_for_oauth_by_email(email, resource)
      elsif uid && name
        user = find_for_oauth_by_uid(uid, resource)
        if user.nil?
          user = find_for_oauth_by_name(name, resource)
        end
      end
    else
      user = resource
    end

    auth = user.authorizations.find_by_provider(provider)
    if auth.nil?
      auth = user.authorizations.build(:provider => provider)
      user.authorizations << auth
    end
    auth.update_attributes auth_attr
    if auth_attr[:first_name]&&!auth_attr[:first_name].empty?&&auth_attr[:last_name]&&!auth_attr[:last_name].empty? then
      user.name = auth_attr[:first_name].to_s + " " + auth_attr[:last_name].to_s
    end
    if auth_attr[:link] && !auth_attr[:link].empty? then
      user.linkedin_link = auth_attr[:link]
    end 
    if auth_attr[:location] && !auth_attr[:location].empty? then
      user.location = auth_attr[:location]
    end 
    user.save
    return user
  end

  def find_for_oauth_by_uid(uid, resource=nil)
    user = nil
    if auth = Authorization.find_by_uid(uid.to_s)
      user = auth.user
    end
    return user
  end

  def find_for_oauth_by_email(email, resource=nil)
    if user = User.find_by_email(email)
      user
    else
      user = User.new(:email => email, :password => Devise.friendly_token[0,20])
      user.save
    end
    return user
  end

  def find_for_oauth_by_name(name, resource=nil)
    if user = User.find_by_name(name)
      user
    else
      first_name = name
      last_name = name
      user = User.new(:first_name => first_name, :last_name => last_name, :password => Devise.friendly_token[0,20], :email => "#{UUIDTools::UUID.random_create}@host")
      user.save(:validate => false)
    end
    return user
  end

end