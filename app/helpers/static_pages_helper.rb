module StaticPagesHelper
  include ActionView::Helpers::TextHelper
  def simple_format_nbsp (t, tag = "p")
    return simple_format(t.to_s.gsub(/\s(?<word>\S{1,3})\s/,' \k<word>&nbsp;'), {}, wrapper_tag: tag)
  end
  
end
