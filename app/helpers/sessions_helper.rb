module SessionsHelper
=begin	
  # Logs in the given user.
  def log_in(user)
    puts "Log in " + user.id.to_s
    session[:user_id] = user.id
    lh = user.log_history
    if lh==nil then lh=[] end
    lh << Time.zone.now
    user.update_attribute(:lastlogged_at, Time.zone.now)
    user.update_attribute(:log_history, lh)
  end
  
  # Remembers a user in a persistent session.
  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end
  
  def current_user
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id: user_id)
      if user && user.authenticated?(:remember, cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end
  
    # Returns true if the user is logged in, false otherwise.
  def logged_in?
    !current_user.nil?
  end
  def current_user_name
    if logged_in?
      if !current_user.name.nil?
        current_user.name
      else
        current_user.email
      end
    else nil
    end
  end
  
  # Forgets a persistent session.
  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

  # Logs out the current user.
  def log_out
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil

  end
=end

   # Redirects to stored location (or to the default).
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # Stores the URL trying to be accessed.
  def store_location
    session[:forwarding_url] = request.url if request.get?
  end

end
