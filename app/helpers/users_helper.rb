module UsersHelper
  $globalvar_skills = ["fullstack", "frontend", "backend", "techlead", "it engineer", "highload", "ruby on rails", "responsive", "sass", "javascript", "jquery", "ajax", "angularjs", "reactjs", "python", "php", "perl", "magento", "ios", "swift", "objective c", "android", "xamarin", "scala", "dotnet", "java", "C/C++", "mysql", "postresql", "sphinx", "redis", "kendo", "api", "sap",  "sql", "erp", "machine learning", "big data", "hadoop", "mapreduce", "amazonws", "data science", "web design", "ux ui design", "graphic design", "product design", "illustration", "photo", "video", "web analytics", "business analytics", "product management", "scrum", "bizdev", "financial modeling", "growth hacking", "market analytics", "git", "mongo db", "neural networks", "deep learning", "django", "nodejs", "opengl", "flux", "backbonejs", "NLP", "golang", "docker"].sort
  
  $globalvar_test = [
    [0, "iOS & Objective C", 300, [2,3,2,4,2,3,4,4],
      [["What is the difference between accessing a property via self or underscore (_)?", 
        ["Underscore triggers KVO",
        "Self triggers getter and setter",
        "Underscore is preferrable and should be used every time",
        "There is no difference"]], 
      ["What is the difference between weak and strong references?", 
        ["Weak creates and changes a copy of the object",
        "Weak reference cannot override value of the object as long as a strong reference exist",
        "Weak reference will be set to null automatically when all strong references are destroyed",
        "Only one strong reference can exist for each object simultaneously"]], 
      ["We have three objects: a grandparent, parent and child. The grandparent retains the parent, the parent retains the child and the child retains the parent. The grandparent releases the parent, what happens? (No other references exist.)", 
        ["The parent is destroyed, the child remains",
        "Both the parent and the child remain",
        "All three objects are destroyed",
        "Only the child is destroyed"]], 
      ["What is method swizzling?", 
        ["Changing implementation of a standard method",
        "Checking if the method is available",
        "Checking if the method is overriden",
        "Changing implementation of a standard selector"]], 
      ["What is ARC?", 
        ["Automatic Reference Changing",
        "Automatic Reference Counting",
        "Automatic Reference Cleaning",
        "Automatic Reference Cooling"]], 
      ["What is NOT a part of the Core Data Stack?", 
        ["The managed object model",
        "The managed object context",
        "The persistent store model",
        "The persistent store coordinator"]], 
      ["dispatch_once ...", 
        ["... destroys the object but keeps its children alive",
        "... sends a request",
        "... creates an object without a token",
        "... is a method from GCD"]], 
      ["What is the difference between @dynamic and @synthesize?", 
        ["@synthesize tells the compiler that the getter and setter methods are not implemented",
        "@synthesize tells the compiler that the getter and setter methods are implemented by a superclass",
        "@dynamic tells the compiler that the getter and setter methods are implemented by the class itself",
        "@dynamic tells the compiler that the getter and setter methods are implemented not by the class itself, but somewhere outside"]]]
    ],
    
    [1, "iOS & Objective C - 2", 300, [2,3,1,3,4,4,2,3],
      [["Which is typically used to avoid Retain Cycles?", 
        ["Strong attribute in a reference to a parent",
        "Weak attribute in a reference to a parent",
        "Copy attribute in a reference to a parent",
        "Strong attribute in a reference to a child"]], 
      ["Which is the reason for declaring a Category instead of a Subclass?", 
        ["To use methods from the original class",
        "To override methods from the original class",
        "To add new methods to all instances of the original class",
        "There is no reason"]], 
      ["What is true about the difference between a Category and a Class Extension?", 
        ["A Class Extention can only be added to a class that is compiled at the same time as the Extention",
        "A Category is just an anonymous Class Extention",
        "A Category can add its own properties and instance variables to a class, while a Class Extention cannot",
        "There are no differences"]], 
      ["Which statement is true about views?", 
        ["Your view may have any number of superviews",
        "Sibling views may not overlap each other in iOS",
        "When the content of a view changes, it is your responsibility to notify the system that your view needs to be redrawn",
        "Block-based animations are available only in iOS 7 and later"]], 
      ["What is true about the Size Classes?", 
        ["A class is a Size Class if all its instances take up the same space in memory",
        "Size Classes are large data sctructures that take up substantial memory",
        "There is no such thing as Size Classes",
        "Size Classes enable developers to stop thinking in terms of specific devices and their screen sizes"]], 
      ["What is true about @autorelease?", 
        ["Autorelease fully automatically manages memory allocation issues",
        "Your code may send an autorelease message outside of its autorelease pool block",
        "For a given autorelease message, the release message is sent immediately, not at the end of the autorelease pool block",
        "When you create a large number of temporary objects that you want to dispose of more quickly, you should create your own autorelease pool block"]], 
      ["What best describes a Managed Object Context?", 
        ["A Managed Object Context is an unique object that stores all instances of your app's Managed Objects",
        "A Managed Object instance exists in one and only one Context, but multiple copies of an Object can exist in different Contexts",
        "A Managed Object Context stores unedited copies of instances of Managed Objects loaded from a Persistent Store",
        "A Managed Object Context is any collection of Managed Objects"]], 
      ["What is true about multithreading in iOS?", 
        ["Creating a new thread is relatively cheap in iOS as it does not require any interactions with the kernel",
        "Cocoa framework by default creates locks and other forms of internal synchronization even for single-threaded applicaionts, degrading their performance",
        "In iOS and OS X v10.5 and later, all objects have the ability to spawn a new thread and use it to execute one of their methods",
        "A detached thread means that the thread’s resources will not be reclaimed by the system when the thread exits, and that you have to take cake of this yourself"]]]
    ]
  ]
  
  def check_connection(provider)
    if current_user.has_connection_with(provider)
      html = ""
    else
      html = link_to user_omniauth_authorize_path(provider: provider.downcase), class: "#{provider}-connect" do
        content_tag :span, 'Connect with ' + provider.to_s, class: "un-verified"
      end
    end
  end
end
