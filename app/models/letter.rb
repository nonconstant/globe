class Letter < ActiveRecord::Base
  include ActionView::Helpers::TextHelper
  serialize :skills
    
  def send_plain_mail
    UserMailer.send_plain_mail(rec, header, text).deliver_now
    
  end
  #Sends letter to 0 - all, 1 - pros, 2 - businesses, 3 - only to Irina (test)
  
  def send_mail_danger
    case self.category
    when 1
      recipients = User.where(utype: 0)
    when 2
      recipients = User.where(utype: 1)
    when 3
      recipients = User.where(id: 232)
    else
      recipients = User.all
    end
    recipients.each do |rec|
      send = true
      if self.skills && !self.skills.empty? then
        send = false
        self.skills.each do |ss|
          if rec.skills && !rec.skills.empty? && rec.skills.include?(ss[0]) then
            send = true
          end
        end
      end
      if (rec.stop_letters.to_i!=1) && (rec.id >= start_id.to_i) && send then
        puts "SEND MAIL TO " + rec.email
        UserMailer.send_mail(rec.email, rec.name, header, text).deliver_now
      end
    end
    
  end
end
