class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable, :omniauthable, :omniauth_providers => [:facebook, :linkedin]
         
  has_many :authorizations, dependent: :destroy
  has_many :offers, dependent: :destroy
  has_many :tests, dependent: :destroy
  include ActionView::Helpers::TextHelper
  acts_as_voter
  acts_as_votable
  acts_as_messageable
  
  #attr_accessor :remember_token, :activation_token, :reset_token
  serialize :likes
  serialize :skills
  serialize :log_history
  serialize :update_history
  serialize :recommended

  def has_connection_with(provider)
    auth = self.authorizations.where(provider: provider).first
    if auth.present?
      auth.token.present?
    else
      false
    end
  end
  
  def self.from_omniauth(auth, type)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      puts "from omniauth"
      puts auth.info.to_s
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.name = auth.info.name
      user.picture = auth.info.image 
    end
  end

  def send_interest_email (receiver)
    UserMailer.express_interest(self, receiver).deliver_now
  end
  
  def send_mutual_interest_email (receiver)
    UserMailer.mutual_interest(self, receiver).deliver_now
  end
  
  def has_test?(num)
    if tests
      tests.each do |test|
        if test.num == num then 
          return true 
        end
      end
    end
    return false
  end
  
  def recalc_all_ratings
    User.all.each do |u|
      u.calcrating
    end
  end
  
  def calcrating
    r = 0
    if name then r+=50 end
    if resume&&resume.length>2 then r+=20 end
    if values&&values.length > 20 then r+=10 end
    if experience&&experience.length > 20 then r+=10 end
    if educ&&educ.length > 20 then r+=10 end
    if linkedin_link&&linkedin_link.length>2 then r+=1 end
    if github_link&&github_link.length>2 then r+=1 end
    if facebook_link&&facebook_link.length>2 then r+=1 end
    self.rating = r
    self.save
  end
  
  def add_recommendation(id)
    if !self.recommended then
      self.recommended = []
      self.recommended << id
      save
    elsif !self.recommended.include? id then
      self.recommended << id
      save
    end
  end
  
  def del_recommendation(id)
    if !self.recommended then
      self.recommended = []
      save
    elsif self.recommended.include? id then
      self.recommended.delete(id)
      save
    end
  end
  
  def username(id)
    if !User.exists?(id)
      return "User not found"
    end
    return User.find(id).name
  end
    
  
  private
  
  # Converts email to all lower-case.
  def downcase_email
    self.email = email.downcase
  end
  
  
end
