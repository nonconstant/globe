json.array!(@users) do |user|
  json.extract! user, :id, :name, :skype, :linkedin, :roles, :values, :places, :status
  json.url user_url(user, format: :json)
end
