class ApplicationMailer < ActionMailer::Base
  default from: "The Globe <theglobeteam@gmail.com>"
  layout 'mailer'
end
