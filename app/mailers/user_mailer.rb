class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.account_activation.subject
  #
  def send_mail (email, name, title, text)
    @name = name
    @title = title
    @text = text
    mail to: email, subject: title, cc: "theglobeteam@gmail.com"
    puts "sent mail text = " + text.to_s
  end
  def send_plain_mail (email, header, text)
    @text = text
    mail to: email, subject: header, from: "Irina Nazarova <ienazarova@gmail.com>"
  end
  
  def express_interest (sender, receiver)
    @sender = sender
    @receiver = receiver
    @title = []
    if receiver.utype == 0 && sender.utype == 1 then
      location = "The Globe"
      if sender.location!=nil && sender.location!=[] then location = sender.location.to_s end
      @title = "A project from " + location + " is interested in hiring you. Let's chat!"
    elsif receiver.utype == 1 && sender.utype == 0 then
      @title = sender.name + " is interested in working with you. Review the profile."
    end
    mail to: receiver.email, subject: @title, cc: "theglobeteam@gmail.com"
  end
  
  
  def mutual_interest (business, pro)
    @business = business
    @pro = pro
    mail to: "theglobeteam@gmail.com", subject: "Mutual interest expressed - The Globe!"
  end
  def account_activation (user)
    @user = user
    @title = "Account activation"
    mail to: user.email, subject: @title
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #
  def password_reset(user)
    @user = user
    @title = "Password reset"
    mail to: user.email, subject: @title
  end
  
  def notification (user, message)
    @user = user
    @message = message
    mail to: user.email, subject: "Notification from The Globe", cc: "theglobeteam@gmail.com"
  end
  def notification2 (user1, user2, message)
    @user = user
    @message = message
    mail to: user1.email, subject: "Notification from The Globe", cc: "theglobeteam@gmail.com"
  end
  
end
